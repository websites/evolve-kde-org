---
layout: page
title: How do we get there?
---
In order to provide the KDE community with the means to assess its current position and find future direction we have devised this yearly iterative process:

* First, we will gather extensive input from the wider community: everyone from core contributors to casual contributors to users. This will be done via various means, the main one being a survey, but also including forums, mailing lists, IRC office hours and in-person meetings; for example at Akademy.
* This input is then consolidated into a report. It is going to be published before Akademy for public consumption. This report will summarize community conclusions and potential areas of focus and improvement.
* During KDE e.V.'s annual general assembly, the report is discussed and some of the recommended focus areas are agreed on as goals.
* At a strategy sprint, core community members come up with measureable suggestions to achieve those goals.
* Finally, there will be a wrap up session that will evaluate how much progress we have made towards the goals we set ourselves. The evaluation will be presented at the next general assembly meeting.

KDE e.V. will support this process and its outcome. The outcome of this process are happy contributors and happy users.