---
layout: page
title: Shaping our future
---
KDE began its life as a desktop project and Qt showcase back in 1996. Since then KDE has evolved to become something more significant; the modern KDE is a global community of technologists, designers, writers and advocates producing some of the world's finest user-centric Free Software.

As we have evolved, so too has the world around us. The user's experience is no longer restricted to the desktop. It has expanded to the user's hands, wrists, glasses and more and will continue to evolve into areas we have yet to imagine.

In KDE we want to be in charge of our future. We want  to have a clear and honest approach for reacting to and influencing our shifting environment, to continuously and consciously improve. We want to do what is necessary to be the thriving community for creating technology that will satisfy the needs of the next 20 year's users.

In order to shape our evolution it is crucial that the wider KDE community understands its current position and where it aims to be in the future. As the primary support structure within the KDE community, KDE e.V. is instrumental in guiding that journey.

Through regular honest assessment and reaction to our environment the KDE community continues to remain effective and relevant and ensures that KDE's users will continue to Experience Freedom.


<div style="text-align: center;"><a href="http://survey.kde.org/index.php/survey/index?sid=752944&lang=en">Take the survey</a></div>