---
layout: page
title: Survey Results
---
Earlier this year we started Evolving KDE in order to reflect on where we as a community stand and where we want to go. The first step was a <a href="http://survey.kde.org/index.php/survey/index?sid=752944&lang=en">survey</a>.

The evaluation and the recommendations based on the findings of the survey are available <a href="surveyresults.pdf">here</a>.
